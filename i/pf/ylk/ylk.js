$(document).ready(function(){

   $(window).resize(function(){
      resizer();
   });
   resizer();

   $('.one-preview').click(function(){
      $('.one-preview').removeClass("active");
      $(this).addClass('active');
      $('.absolute-img').css("background-image","url('"+$(this).attr("big")+"')");
   });
});


function resizer(){
   setTimeout(function(){
      var width = $('body').width();

      var p = 0;
      if (width<1200){
         $('.left-line').css("width", "20px");
         $('.right-line').css("width", (width-20-81)+"px");
         $('.padding-left').css("padding-left","40px");
         $('.map .padding-left').eq(0).css("padding-left","0");
         
         $('.col-blue').css('font-size','27px');
         $('.col-blue').css('line-height','35px');
         $('.gray-col').css('font-size','18px');
         $('.gray-col').css('line-height','24px');
         
         $('.col-blue').css('padding','0 10px');
         $('.gray-line').css('margin','0 10px');
         $('.gray-col').css('padding','0 10px');
         $('.right-text').css("width", (width-40-665+160)+"px");
         
         $('.full2').css('background-position','50% 260px');
         
         $('.full3').css('background-position','50% 260px');
         $('.head1').css('background-position','-250px 0');
         $('.one-col').css("margin-right", '0');
         p=260;
      } else {
         $('.head1').css('background-position','50% 0');
         $('.left-line').css("width", "140px");
         $('.right-line').css("width", (width-140-81)+"px");
         $('.padding-left').css("padding-left","190px");
         $('.col-blue').css('font-size','40px');
         $('.col-blue').css('line-height','50px');
         $('.gray-col').css('font-size','24px');
         $('.gray-col').css('line-height','30px');
         $('.gray-col').css('padding','0');
         $('.col-blue').css('padding','0');
         $('.gray-line').css('margin','0');
         $('.one-col').css("margin-right", '1%');
         $('.right-text').css("width", (width-190-665)+"px");
         
         $('.full2').css('background-position','50% 260px');
      }
      
      $('.book-bg').css("min-height", 0.9*width/1.8+"px");
      
      $('.full2').css("min-height", p+width/1.67+"px");
      $('.full3').css("height", p+874+"px");
      
   },10);

}